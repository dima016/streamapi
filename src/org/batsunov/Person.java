package org.batsunov;

import org.batsunov.Animal;

import java.util.ArrayList;
import java.util.List;


public class Person {
    private String name;
    private List<Animal> animals = new ArrayList<>();

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Animal> getAnimals() {
        return animals;
    }


    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", animals=" + animals +
                '}';
    }
}
