package org.batsunov;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersonStreamOption {


    public static void printAllAnimal(Stream<Person> stream) {
        stream.flatMap(human -> human.getAnimals().stream()).forEach(System.out::println);
    }


    public static List<Animal> listOfAnimal(Stream<Person> stream) {
        return stream.flatMap(human -> human.getAnimals().stream())
                .collect(Collectors.toList());
    }


    public static List<? extends Animal> listOfDog(Stream<Person> stream) {

        return stream.flatMap(human -> human.getAnimals().stream())
                .filter(Dog.class::isInstance)
                .collect(Collectors.toList());

    }


    public static double avgAge(Stream<Person> stream) {

        return stream.flatMap(human -> human.getAnimals()
                .stream())
                .filter(animal -> Cat.class.isInstance(animal) && (!"Tom".equals(animal.getName())))
                .mapToInt(Animal::getAge)
                .average()
                .orElseThrow();
    }


    public static void voiceOlderFour(Stream<Person> stream) {

        stream.flatMap(human -> human.getAnimals().stream())
                .filter(animal -> animal.getAge() >=4)
                .forEach(animal -> System.out.println(animal.voice()));
    }


    public static List<Person> ownersMoreTwoAnimals(Stream<Person> stream) {

        return stream.filter(human -> human.getAnimals().size() > 2)
                .collect(Collectors.toList());

    }


    public static List<String> listOfNameCats(Stream<Person> stream) {

        return stream.flatMap(human -> human.getAnimals().stream())
                .filter(animal -> Cat.class.isInstance(animal))
                .map(Animal::getName)
                .sorted()
                .distinct()
                .collect(Collectors.toList());

    }


    public static List<String> listOfUpperNameDogs(Stream<Person> stream) {

        return stream.flatMap(human -> human.getAnimals().stream())
                .filter(animal -> Dog.class.isInstance(animal))
                .map(animal -> animal.getName().toUpperCase())
                .collect(Collectors.toList());

    }


    public static Map<String, String[]> mapPerson(Stream<Person> stream) {

        return stream.collect(Collectors.toMap(Person::getName, human -> human.getAnimals().stream()
                .map(Animal::getName).
                        toArray(String[]::new)));
    }


    public static Optional<Animal> getAnimalMoreFiveYears(Stream<Person> stream) {

        return stream.flatMap(human -> human.getAnimals().stream())
                .filter(animal -> animal.getAge() > 5)
                .findAny();
    }

}