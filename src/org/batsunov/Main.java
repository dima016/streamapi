package org.batsunov;

import java.util.Iterator;
import java.util.Map;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Person vito = new Person("Vito");
        vito.getAnimals().add(new Dog("SHARIK", 3));
        vito.getAnimals().add(new Cat("Tom", 5));

        Person michael = new Person("Michael");
        michael.getAnimals().add(new Cat("Pushok", 1));
        michael.getAnimals().add(new Dog("Barsik", 4));
        michael.getAnimals().add(new Dog("Sharik", 5));

        Person sarah = new Person("Sarah");
        sarah.getAnimals().add(new Cat("KIKI", 3));
        sarah.getAnimals().add(new Cat("Tom", 5));
        sarah.getAnimals().add(new Dog("PIII", 10));
        sarah.getAnimals().add(new Dog("Tiki", 8));

        Stream<Person> personStream = Stream.of(vito, michael, sarah);

        //  PersonStreamOption.listOfAnimal(personStream);
        PersonStreamOption.printAllAnimal(personStream);
        //3 System.out.println(PersonStreamOption.listOfDog(personStream));
        // System.out.println(PersonStreamOption.avgAge(personStream));
        //5 PersonStreamOption.voiceOlderFour(personStream);
        //6 System.out.println(PersonStreamOption.ownersMoreTwoAnimals(personStream));
        //7 System.out.println(PersonStreamOption.listOfNameCats(personStream));
        //8 System.out.println(PersonStreamOption.listOfUpperNameDogs(personStream));

        //    Animal animal = PersonStreamOption.getAnimalMoreFiveYears(personStream).get();
        //  System.out.println(animal);

    }
}
