package org.batsunov;

public interface Animal {
    String getName();
    String voice();
    int getAge();
}
